# Windows vs Linux vs Windows VM

GPU performance comparison between Windows bare metal, VM (single GPU passthrough) and Arch Linux bare metal.

## System Specs

| Component | Actual hardware |
| --------- | --------------- |
| CPU | AMD Ryzen 5600x |
| GPU | Nvidia GTX 1080 Watercooled |
| RAM | 32 Gb Corsair Vengeance 3200 MHZ |
| MOBA | Asus Strix B550i Gaming |
| PSU | Corsair SF600 Platinum |

Proprietary drivers on Loonix.

idk nothing else should matter.

## Bench settings

| Settings | |
| ------ | ------ |
| Mode | 1920x1080 8xAA fullscreen |
| Preset | Extreme HD |

but monitor is 2560x1080. Shouldn't matter idk.

## Actual table

### OpenGL

| System | Score (diff) | FPS | Min FPS | Max FPS |
| ------ | ----- | --- | ------- | ------- |
| Win Metal | 4161 | 99.5 | 43.2 | 191.2 |
| Win VM | 4100 (-2%) | 98 | 31.9 | 189.6 |
| Linux | 3796 (-9.6%) | 90.7 | 47.7 | 152.4 |

### D3D11

| System | Score | FPS | Min FPS | Max FPS |
| ------ | ----- | --- | ------- | ------- |
| Win Metal | 4774 | 114.1 | 36.3 | 238.4 |
| Win VM | 4532 (-5%) | 108.3 | 19.5 | 229.2 |
